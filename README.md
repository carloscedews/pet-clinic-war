# Despliegue de Aplicación Monolitica (WAR) PetClinic

1. Se dispone del proyecto PetClinic para generar el WAR a desplegar 
2. Se ha creado un repositorio en Bitbucket publico y se alojo la aplicación https://bitbucket.org/carloscedews/
3. Se ha creado un pipeline(Jenkinsfile) con los siguientes stages:
- Build (culminado al 100%)
- Testing(Junit + Jacoco) (pendiente por falta de aplicativo WAR con pruebas unitarias)
- Sonar (cobertura mayor a 60%) (pendiente por falta de aplicativo WAR con pruebas unitarias)
- Artifactory (pendiente por falta de aplicativo WAR con pruebas unitarias)
- Despliegue en Jboss(Ansible) (culminado al 100%)

​Se preparó un entorno usando una instancia Ubuntu 20.04 en el proveedor de nube Digital Ocean para instalar Jenkins (http://147.182.231.193:8080/)
Tambien se ha utilizado en el proveedor de nube AWS para crear la instancia de Centos 8 con java11 y JBoss EAP 7.3 como servidor de aplicaciones en donde se desplegará el WAR  en la siguiente URL http://54.160.225.106:8080/spring-petclinic-2.3.1.BUILD-SNAPSHOT/, cabe indicar que la IP de centos cambia al detener y reiniciar la instancia gratuita en AWS.

![Screenshot](monolitico1.png)
